const {Router} = require("express")
const satpam = require('./middlewares/AuthMiddleware')

const router = Router()
router.get('/', satpam, (req, res) => {
    res.render('dashboard')
})

router.get('/list-cars', satpam,(req, res)=>{
    res.render("list-car")
})

router.get('/add-cars', satpam,(req, res)=>{
    res.render("add-new-car")
})

module.exports = router